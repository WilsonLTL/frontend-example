// Library
import { useSelector } from "react-redux";
import { useRouter } from "next/router";

// Page Compontent
import CenterContainerComponent from "./centerContainerComponent"

export const CenterContainer = () => {
  const router = useRouter();
  const articlePage = useSelector((state: any) => state.articlePageReducer);

  return (<CenterContainerComponent router={router} articlePage={articlePage}/>)
}