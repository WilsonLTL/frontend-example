// React
import React, { useEffect } from "react";
// Mui
import { IconButton } from "@mui/material";
import { ArrowBackIos } from "@mui/icons-material";
// Library
import parse from "html-react-parser";
// Function
import { onClickBackListener } from "./function"

const centerContainerComponent = ({ router, articlePage }) => {

  useEffect(() => {
    if (articlePage === undefined || articlePage.article === undefined) router.push('/')
  }, articlePage)
  
  return (
    <>
      <div className="flex items-center justify-between w-full bg-white border shadow h-14">
        <IconButton onClick={()=>onClickBackListener(router)} className="ml-4 rounded-full bg-green-50">
          <ArrowBackIos className="text-green-900" />
        </IconButton>
      </div>
      <div className="p-12 border shadow">
        {articlePage !== undefined &&
          articlePage.article !== undefined &&
          parse(articlePage.article.content)}
      </div>
    </>
  );
};

export default centerContainerComponent;
