// React
import React, { useEffect } from "react";
// Mui
import { Button, Skeleton, Pagination } from "@mui/material";
import { BookmarkBorderOutlined, HelpOutline, Replay } from "@mui/icons-material";
// Actions
import { INIT_DATA } from "../../../../store/actions/homePageActions";
// Type
import { article } from "../../../../store/types/homePageType"
// GraphQl
import { useLazyQuery } from '@apollo/client'
import { articlesList } from "../../../../apis/graphQL"
// Function 
import { onClickArticleListener } from "./function"

const ArticleListCompontent = ({ t, dispatch, router, homePage }) => {
  // Only avaible to using useState when the compontent is a private compontent
  const [page, setPage] = React.useState(1);
  const handleChange = (event, value) => { setPage(value) };
  const [getArticlesList, { data, loading, error }] = useLazyQuery(articlesList)

  useEffect(() => {
    if(!error && !loading && data) {
      // Init data
      dispatch({
        type: INIT_DATA,
        payload: {
          articleList: data.articles
        }
      })
    }
  }, [data, error, loading])

  return (
    <>
      <div className="flex items-center justify-between px-4 py-4">
        <div className="flex space-x-4">
          <Button className="bg-green-50 font-bold text-base text-green-900 rounded-3xl">
            { t('trending') }
          </Button>
          <Button className="hover:bg-gray-100 font-bold text-base text-gray-400 rounded-3xl">
            { t('latest') }
          </Button>
          <Button className="hover:bg-gray-100 font-bold text-base text-gray-400 rounded-3xl">
            { t('featured') }
          </Button>
        </div>
        <div className="flex space-x-2">
          <Button onClick={()=>getArticlesList()} className="space-x-1 rounded-3xl">
            <Replay className="text-gray-400 w-5 h-5"/>
            <span className="text-gray-400 text-sm font-bold">{ t('shuffle') }</span>
          </Button>
          <Button className="space-x-1 rounded-3xl">
            <HelpOutline className="text-gray-400 w-5 h-5"/>
            <span className="text-gray-400 text-sm font-bold">{ t('help') }</span>
          </Button>
        </div>
      </div>
      { homePage.articleList === undefined ?
        [...Array(6)].map(() => {
          return (
            <div
              className="border-b p-4 space-y-2"
              key={
                Math.random().toString(36).substring(2, 15) +
                Math.random().toString(36).substring(2, 15)
              }
            >
              <Skeleton />
              <div className="flex space-x-2">
                <Skeleton variant="circular" width={25} height={25} />
                <Skeleton width={100} />
              </div>
              <Skeleton />
              <div
                className="flex items-center justify-between"
                key={
                  Math.random().toString(36).substring(2, 15) +
                  Math.random().toString(36).substring(2, 15)
                }
              >
                <div className="flex space-x-2">
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                </div>
                <BookmarkBorderOutlined className="text-gray-400 w-5 h-5" />
              </div>
            </div>
          );
        }) : 
        homePage.articleList.slice ((page-1)*5, page*5).map((article: article) => {
          return (
            <div
              className="border-b p-4 space-y-2 cursor-pointer hover:bg-gray-100"
              key={
                Math.random().toString(36).substring(2, 15) +
                Math.random().toString(36).substring(2, 15)
              }
              onClick={()=>onClickArticleListener(dispatch, router, article)}
            >
              <div className="flex flex-col space-y-2">
                <h2 className="text-black text-xl font-bold">{ article.title }</h2>
                <div className="flex space-x-2">
                  <Skeleton variant="circular" width={25} height={25} />
                  <p className="text-sm">{ article.user_id }</p>
                </div>
                <p className="text-gray-400 text-sm truncate">{ article.description }</p>
                <div
                  className="flex items-center justify-between"
                  key={
                    Math.random().toString(36).substring(2, 15) +
                    Math.random().toString(36).substring(2, 15)
                  }
                >
                  <div className="flex space-x-2">
                    <Skeleton width={100} />
                    <Skeleton width={100} />
                    <Skeleton width={100} />
                  </div>
                  <BookmarkBorderOutlined className="text-gray-400 w-5 h-5" />
                </div>
              </div>
            </div>
          )
        })
      }
      {
        homePage.articleList !== undefined && 
        <div className="flex justify-center items-center">
          <Pagination className="flex p-3" count={Math.ceil(homePage.articleList.length/5)} page={page} onChange={handleChange} />
        </div>
      }
    </>
  );
};

export default ArticleListCompontent;
