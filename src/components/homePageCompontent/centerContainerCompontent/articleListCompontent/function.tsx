import { UPDATE_ARTICLEPAGE_ARTICLE } from "../../../../store/actions/articlePageActions"

export function onClickArticleListener (dispatch, router, article) {
  dispatch({
    type: UPDATE_ARTICLEPAGE_ARTICLE,
    payload: {
      article: article
    }
  })
  router.push(`/article/${article.title}-${article.user_id}`)
}