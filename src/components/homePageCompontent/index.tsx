// Library
import { useDispatch, useSelector } from "react-redux";
import UseTranslation from "next-translate/useTranslation";
import { useRouter } from "next/router";

// Page Compontent
import CenterContainerCompontent from "./centerContainerCompontent"
import LeftContainerCompontent from "./leftContainerCompontent"
import RightContainerCompontent from "./rightContainerCompontent"

export const CenterContainer = () => {
  const { t } = UseTranslation("common");
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector((state: any) => state.userReducer);
  const homePage = useSelector((state: any) => state.homePageReducer);

  return (<CenterContainerCompontent t={t} dispatch={dispatch} router={router} user={user} homePage={homePage}/>)
}

export const LeftContainer = () => {
  const { t } = UseTranslation("common");
  const user = useSelector((state: any) => state.userReducer);
  const router = useRouter();

  return (<LeftContainerCompontent t={t} user={user} router={router}/>)
}

export const RightContainer = () => {
  const { t } = UseTranslation("common");

  return (<RightContainerCompontent t={t}/>)
}