// React
import React from "react";
// Mui
import { Button, Skeleton } from "@mui/material";
import { ArrowForwardIos, Replay } from "@mui/icons-material";

export const AuthorsCompontent = ({ t }) => {
  return (
    <div className="flex flex-col space-y-4">
      <div className="flex items-center justify-between">
        <span className="text-xl font-bold">{t("authors")}</span>
        <div>
          <Button className="space-x-1 rounded-3xl">
            <Replay className="text-gray-400 w-4 h-4" />
            <span className="text-gray-400 text-xs font-bold">
              {t("shuffle")}
            </span>
          </Button>
          <Button className="space-x-1 rounded-3xl">
            <span className="text-gray-400 text-xs font-bold">
              {t("show_more")}
            </span>
            <ArrowForwardIos className="text-gray-400 w-4 h-4" />
          </Button>
        </div>
      </div>
      {[...Array(5)].map(() => {
        return (
          <div
            className="flex space-x-2"
            key={
              Math.random().toString(36).substring(2, 15) +
              Math.random().toString(36).substring(2, 15)
            }
          >
            <Skeleton variant="circular" width={50} height={50} />
            <div className="flex flex-col w-full">
              <Skeleton />
              <Skeleton />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default AuthorsCompontent;
