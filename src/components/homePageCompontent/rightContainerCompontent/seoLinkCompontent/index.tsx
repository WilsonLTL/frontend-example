// React
import React from "react";
// Mui
import { Link } from '@mui/material'; 

const SeoLinkCompontent = ({ t }) => {

  return (
    <div className="space-x-2">
      <Link
        href="https://matters.news/about"
        underline="none"
        className="text-gray-500 text-sm"
      >
        Matters 長什麼樣
      </Link>
      <Link
        href="https://matters.news/guide"
        underline="none"
        className="text-gray-500 text-sm"
      >
        玩轉 Matters 實用指南
      </Link>
      <Link
        href="https://matters.news/community"
        underline="none"
        className="text-gray-500 text-sm"
      >
        社區共建基地
      </Link>
      <Link
        href="https://matters.news/migration"
        underline="none"
        className="text-gray-500 text-sm"
      >
        一鍵搬家
      </Link>
      <Link
        href="https://matters.news/tos"
        underline="none"
        className="text-gray-500 text-sm"
      >
        用戶協議
      </Link>
      <Link
        href="https://github.com/thematters/developer-resource"
        underline="none"
        className="text-gray-500 text-sm"
      >
        開放社區
      </Link>
      <Link
        href="https://matters.news/@1ampa55ag3/guidance-如何让你的matters之旅更便捷-bafyreiayiuxi4qc2a7qpgjp3fe42wmaoppqykckcvtq4hiukl5pgs3dn2m"
        underline="none"
        className="text-gray-500 text-sm"
      >
        下載應用
      </Link>
    </div>
  );
};

export default SeoLinkCompontent;
