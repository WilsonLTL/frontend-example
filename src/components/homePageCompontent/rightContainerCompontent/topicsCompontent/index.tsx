import { Button, Skeleton  } from '@mui/material';
import { ArrowForwardIos, Replay } from '@mui/icons-material';

const TopicsCompontent = ({ t }) => {
  return (
    <div className="flex flex-col space-y-4">
      <div className="flex items-center justify-between">
        <span className="text-xl font-bold">{ t('topics') }</span>
        <div>
          <Button className="space-x-1 rounded-3xl">
            <Replay className="text-gray-400 w-4 h-4" />
            <span className="text-gray-400 text-xs font-bold">{ t('shuffle') }</span>
          </Button>
          <Button className="space-x-1 rounded-3xl">
            <span className="text-gray-400 text-xs font-bold">{ t('show_more') }</span>
            <ArrowForwardIos className="text-gray-400 w-4 h-4" />
          </Button>
        </div>
      </div>
      {[...Array(5)].map(() => {
        return (
          <div
            className="flex flex-col"
            key={
              Math.random().toString(36).substring(2, 15) +
              Math.random().toString(36).substring(2, 15)
            }
          >
            <Skeleton variant="rectangular" width={50} height={30} />
            <div className="flex space-x-2">
              <div className="w-9/12">
                <Skeleton />
                <Skeleton />
                <Skeleton />
              </div>
              <div className="w-3/12">
                <Skeleton height={60} variant="rectangular" />
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default TopicsCompontent;
