// React
import React from "react";
// Mui
import { InputBase, IconButton, Paper  } from '@mui/material';
import { Search } from '@mui/icons-material';

const SearchBarCompontent = ({ t }) => {
  return (
    <section>
      <Paper className="flex px-4 rounded-xl bg-gray-100 shadow-none">
        <IconButton type="submit" aria-label="search">
          <Search className="w-6 h-6" />
        </IconButton>
        <InputBase placeholder={ t('search_articles_tags_and_authors') } />
      </Paper>
    </section>
  );
};

export default SearchBarCompontent;