// React
import React from "react";
// Nextjs
import Image from "next/image";
// Mui
import { Button } from '@mui/material';
import { AddBoxOutlined, Explore, Mode } from '@mui/icons-material';

import { onClickCreaterListener } from "./function"

const LeftContainerCompontent = ({ t, user, router }) => {

  return (
    <div className="hidden sm:flex flex-col border-r w-3/12">
      <section className="flex justify-center items-center">
        <Image alt="" width={125} height={125} src="/matters-icon.jpg" />
      </section>
      <div className="flex flex-col justify-center items-center space-y-4">
        <Button className="space-x-2 w-32">
          <Explore className="text-green-900 w-6 h-6" />
          <span className="text-green-900	text-xl font-bold">{ t('discover') }</span>
        </Button>
        <Button className="space-x-2 w-32">
          <AddBoxOutlined className="text-gray-800 w-6 h-6" />
          <span className="text-black text-xl font-bold">{ t('follow') }</span>
        </Button>
        {
          // Only Function Login User
          user.token !== '' &&
          <Button onClick={() => onClickCreaterListener(router)} className="space-x-2 w-24 bg-green-900 rounded-3xl">
            <Mode className="text-white" />
            <span className="text-white text-xl font-bold">{ t('create') }</span>
          </Button>
        }
      </div>
    </div>
  );
};

export default LeftContainerCompontent;
