// Library
import { useDispatch, useSelector } from "react-redux";
import UseTranslation from "next-translate/useTranslation";
import { useRouter } from "next/router";

// Page Compontent
import CenterContainerCompontent from "./centerContainerCompontent"
import RightContainerCompontent from "./rightContainerCompontent"

export const CenterContainer = () => {
  const { t } = UseTranslation("common");
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.userReducer);
  const createrPage = useSelector((state: any) => state.createrPageReducer);

  return (<CenterContainerCompontent t={t} router={router} dispatch={dispatch} user={user} createrPage={createrPage}/>)
}

export const RightContainer= () => {
  const { t } = UseTranslation("common");
  const dispatch = useDispatch();
  const createrPage = useSelector((state: any) => state.createrPageReducer);

  return (<RightContainerCompontent t={t} dispatch={dispatch} createrPage={createrPage}/>)
}

