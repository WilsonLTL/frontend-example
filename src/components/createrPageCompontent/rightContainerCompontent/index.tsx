// React
import React from "react";
// Mui
import { InputBase, Paper } from "@mui/material";
// Function
import { onChangeTitleListener, onChangeDescriptionListener } from "./function";

const RightContainerCompontent = ({ t, dispatch, createrPage }) => {

  return (
    <div className="flex flex-col border-l items-center w-3/12 px-3 pb-10">
      <div className="pt-10 space-y-6">
        <Paper className="flex flex-col p-4 space-y-2">
          <p className="border-b font-bold pb-2">{ t('title') }</p>
          <InputBase
            value={createrPage.title}
            onChange={(event) => onChangeTitleListener(event, dispatch)}
            placeholder={ t('plase_enter_your_title') }
          />
        </Paper>
        <Paper className="flex flex-col p-4 space-y-2">
          <p className="border-b font-bold pb-2">{ t('summary') }</p>
          <InputBase
            value={createrPage.description}
            onChange={(event) => onChangeDescriptionListener(event, dispatch)}
            multiline
            rows={4}
            placeholder={ t('plase_enter_your_summary') }
          />
        </Paper>
      </div>
    </div>
  );
};

export default RightContainerCompontent;
