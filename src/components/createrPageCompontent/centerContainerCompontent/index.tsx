// React
import React, { useEffect } from "react";
// NextJs
import dynamic from "next/dynamic";
// Mui
import { Button, IconButton } from "@mui/material";
import { ArrowBackIos } from "@mui/icons-material";
// Library
import "react-quill/dist/quill.snow.css";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
// Function
import {
  onClickBackListener,
  onChangeContentListener,
  onClickSubmitListener,
  onOpenSuccessSnackBar,
  onOpenErrorSnackBar,
} from "./function";
// GraphQL
import { useMutation } from "@apollo/client";
import { createArticle } from "../../../apis/graphQL";

const CenterContainerCompontent = ({
  t,
  router,
  dispatch,
  user,
  createrPage,
}) => {
  const [CreateArticle, { loading, error, data }] = useMutation(createArticle);

  useEffect(() => {
    if (data) {
      onOpenSuccessSnackBar(dispatch, router);
    }
    if (error) {
      onOpenErrorSnackBar(dispatch, error);
    }
  }, [loading, error, data]);

  return (
    <div className="flex flex-col w-6/12 h-screen">
      <div className="flex border-b h-14 items-center justify-between w-full bg-white">
        <IconButton
          onClick={() => onClickBackListener(router)}
          className="bg-green-50 ml-4 rounded-full"
        >
          <ArrowBackIos className="text-green-900" />
        </IconButton>
        <Button
          onClick={() =>
            onClickSubmitListener(dispatch, createrPage, user, CreateArticle)
          }
          className="space-x-2 w-20 bg-green-900 rounded-3xl mr-4"
        >
          <span className="text-white font-bold">{t("submit")}</span>
        </Button>
      </div>
      <ReactQuill
        className="w-full h-full"
        value={createrPage.content}
        onChange={(value) => onChangeContentListener(value, dispatch)}
      />
    </div>
  );
};

export default CenterContainerCompontent;
