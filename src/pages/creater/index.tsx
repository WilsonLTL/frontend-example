// Compontent 
import { LeftContainer } from "../../components/homePageCompontent"
import { CenterContainer, RightContainer }  from "../../components/createrPageCompontent"

function creater () {
  
  return (
    <div className="flex">
      <LeftContainer />
      <CenterContainer />
      <RightContainer />
    </div>
  )
}

export default creater