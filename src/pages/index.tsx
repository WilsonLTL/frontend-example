// React 
import React, { useEffect } from "react";
// Compontent 
import {
  LeftContainer,
  CenterContainer,
  RightContainer
} from "../components/homePageCompontent"
// Library
import { useDispatch } from "react-redux";
// GraphQL
import { useQuery } from '@apollo/client'
import { articlesList } from "../apis/graphQL"
import { INIT_DATA } from "../store/actions/homePageActions"

const Home = () => {
  const dispatch = useDispatch();
  const { data, loading, error } = useQuery(articlesList)
  useEffect(() => {
    if(!error && !loading) {
      // Init data
      dispatch({
        type: INIT_DATA,
        payload: {
          articleList: data.articles
        }
      })
    }
  }, [data, error, loading])


  return (
    <div className="flex">
      <LeftContainer />
      <CenterContainer />
      <RightContainer />
    </div>
  );
};

export default Home;
