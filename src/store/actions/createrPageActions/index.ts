export const UPDATE_CREATERPAGE_TITLE = "UPDATE_CREATERPAGE_TITLE";
export const UPDATE_CREATERPAGE_DESCRIPTION = "UPDATE_CREATERPAGE_DESCRIPTION";
export const UPDATE_CREATERPAGE_CONTENT = "UPDATE_CREATERPAGE_CONTENT";

export const updateCreaterPageTitle = () => ({
   type: UPDATE_CREATERPAGE_TITLE
});

export const updateCreaterPageDescription = () => ({
   type: UPDATE_CREATERPAGE_DESCRIPTION
});

export const updateCreaterPageContent = () => ({
   type: UPDATE_CREATERPAGE_CONTENT
})