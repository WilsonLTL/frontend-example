export const UPDATE_USER_TOKEN = "UPDATE_USER_TOKEN"

export const updateUserToken = () => ({
  type: UPDATE_USER_TOKEN
});