export type articlePageType = {
  article: {
    _id: string,
    user_id: string,
    title: string,
    description: string,
    content: string
  }
}