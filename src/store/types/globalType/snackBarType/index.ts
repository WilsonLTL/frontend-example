export type snackBarType = {
    status: boolean,
    message: string,
    type: string,
    action: any
}